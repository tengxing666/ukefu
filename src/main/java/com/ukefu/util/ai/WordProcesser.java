package com.ukefu.util.ai;

public class WordProcesser {
	private StringBuffer strb = new StringBuffer();
	private String content ;
	private int lastend ;
	public WordProcesser(String content) {
		this.content = content ;
	}
	public void process(int begin, int end) {
		if(lastend == 0 && end > 0) {
			strb.append(content.substring(0,end)).append(" ") ;
		}else if(lastend < end){
			strb.append(content.substring(lastend , end)).append(" ") ;
		}
		if(lastend < end) {
			this.lastend = end ;
		}
	} 
	
	public String getContent() {
		if(content!=null && lastend < content.length()) {
			strb.append(" ").append(content.substring(lastend, content.length())) ;
		}
		return strb.toString() ;
	}
}
