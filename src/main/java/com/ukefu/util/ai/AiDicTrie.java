package com.ukefu.util.ai;

import java.io.IOException;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.model.AiUser;

public class AiDicTrie {
	
	private AiDic trie = new AiDic(this);
	
	private BiMap<String,String> dicMap =  HashBiMap.create();
	
	public BiMap<String, String> getDicMap() {
		return dicMap;
	}
	
	/**
	 * 插入 场景 内容
	 * @param content
	 * @param id
	 * @throws IOException
	 * @throws JcsegException 
	 */
	public void insert(String dicid , String id) throws IOException{
		if(dicMap.get(dicid) == null){
			dicMap.put(dicid, id)  ;
		}
	}
	
	/**
	 * 插入 场景 内容
	 * @param content
	 * @param id
	 * @throws IOException
	 * @throws JcsegException 
	 */
	public void insertDic(String content , String id) throws IOException{
		if(dicMap.get(content) == null){
			dicMap.put(content, id)  ;
		}
	}
	/***
	 * 准确匹配对话内容，如果未找到定义，就返回空
	 * @param content
	 * @return
	 * @throws IOException
	 * @throws JcsegException 
	 */
	public String search(ChatMessage message, AiUser aiUser) throws IOException{
		return trie.search(message , aiUser);
	}
	
	/***
	 * 准确匹配对话内容，如果未找到定义，就返回空
	 * @param content
	 * @return
	 * @throws IOException
	 * @throws JcsegException 
	 */
	public String search(String dicid) {
		return dicMap.get(dicid);
	}
	
	public void clean(){
		dicMap.clear();
	}
}
